function [] = check_intersezioni()

    load('./out/data_results.mat');
    global muE; global muM; global mu; global D; global Rs; global Vm; global Re; global Rm; global Wm; global rp; global vp;
    
    for i = 1:1:length([M_Risultati{:,1}])
       D = 384400; rp = 6378+400; Rs = 66281;
       t_andata = M_Risultati{i,1};
       beta = M_Risultati{i,2};
       lambda1 = M_Risultati{i,3};
       V0 = [M_Risultati{i,4}];
       alphaE = M_Risultati{i,5};
       R0 = [rp*cos(beta),rp*sin(beta),0];
       r1= sqrt(D^2+Rs^2 - 2*D*Rs*cos(lambda1)); 
       gamma1 = acos((D-Rs*cos(lambda1))/r1);
       R1 = [-r1*sin(gamma1),r1*cos(gamma1),0];
       vr0 = dot(R0, V0)/norm(R0);
       scarto = 60;
        
       for k = linspace(0.8*t_andata,t_andata-scarto,100000)
            
            x = kepler_U(k, norm(R0), vr0, alphaE);
            [f, g] = f_and_g(x, k, norm(R0), alphaE);
            R = f*R0 + g*V0;
            Rx = R(1);
            Ry = R(2)-D;
            if(norm([Rx,Ry]) < Rs && k < t_andata)
                
                M_Risultati{i,:} = inf;
                fprintf('Orbita non valida');
            end
        end
    end
end

