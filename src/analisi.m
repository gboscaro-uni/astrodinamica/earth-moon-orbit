clc;
tic;
format short g;

%PARAMETRI FISICI

global muE; global muM; global mu; global D; global Rs; global Vm; global Re; global Rm; global Wm; global rp; global vp;
muE = 398600.44; %Costante Gravitazionale Terra [Km3/s2]
muM = 4904; %Costante Gravitazionale Luna [Km3/s2]
Re = 6378; %Raggio Terra [Km]
Rm = 3476/2; %Raggio Luna [Km]
D = 384400; %Distanza Terra-Luna [Km]
Rs = D*(1/81)^(2/5); %Raggio sfera di influenza luna [Km]
Vm = sqrt(muE/D); %Velocita luna [Km/s]
Wm = Vm/D; %Velocita angolare luna [rad/s]

%PARAMETRI ORBITALI

hp = 400; %Altitudine parcheggio [Km]
rp = Re + hp; %Raggio perigeo parcheggio [Km]
vp = sqrt(muE/rp); %Velocita perigeo parcheggio [Km/s]

fprintf('\n --- Caricamento file --- \n');
load('./out/data_results.mat');

%esporta_excel(M_Risultati,'res_excel');

fprintf('\n --- Ricerca risultato ottimale --- \n');

migliore = inf; x_migliore = -1;
for x = 1:1:length([M_Risultati{:,1}])
    dv = M_Risultati{x,13};
    if(dv < migliore)
        migliore = dv; 
        x_migliore = x;
    end    
end

x_migliore = 492;

t_andata = M_Risultati{x_migliore,1};
beta = M_Risultati{x_migliore,2};
lambda1 = M_Risultati{x_migliore,3};
R0 = [rp*cos(beta),rp*sin(beta),0];
V0 = M_Risultati{x_migliore,4};
alphaE = M_Risultati{x_migliore,5};
vetDD = M_Risultati{x_migliore,6};
vetRx_M = M_Risultati{x_migliore,7};
vetRy_M = M_Risultati{x_migliore,8};
vetRx_ret = M_Risultati{x_migliore,9};
vetRy_ret = M_Risultati{x_migliore,10};

fprintf('\n --- Completato --- \n');

graphics(t_andata,R0,V0,alphaE,vetDD,vetRx_M,vetRy_M,vetRx_ret,vetRy_ret);
%animazione(t_andata,beta,lambda1);

toc;