function [] = animazione(t_andata,beta,lambda1)

    fprintf('\n --- Inizio animazione --- \n');
    
    %PARAMETRI FISICI

    global muE; global muM; global mu;
    muE = 398600.44; %Costante Gravitazionale Terra [Km3/s2]
    muM = 4904; %Costante Gravitazionale Luna [Km3/s2]
    Re = 6378; %Raggio Terra [Km]
    Rm = 3476/2; %Raggio Luna [Km]
    D = 384400; %Distanza Terra-Luna [Km]
    Rs = D*(1/81)^(2/5); %Raggio sfera di influenza luna [Km]
    Vm = sqrt(muE/D); %Velocita luna [Km/s]
    Wm = Vm/D; %Velocita angolare luna [rad/s]

    %PARAMETRI ORBITALI

    hp = 400; %Altitudine parcheggio [Km]
    rp = Re + hp; %Raggio perigeo parcheggio [Km]
    vp = sqrt(muE/rp); %Velocita perigeo parcheggio [Km/s]
    wp = vp/rp; %Velocita angolare parcheggio [rad/s]

    figure(1); clf; hold on;
    title('Free Return Orbit Animation');
    xlabel('X [Km]'); ylabel('Y [Km]');
    timestamp = 0.01; dt = 120;
    hRS = []; hM = []; hT = []; hSC = [];
    axis_lim_earth = [-14e+03 14e+03 -11e+03 11e+03];
    axis_lim_full = [-3.7e+05 3.7e+05 -0.95e+5 4.95e+05];
    axis_lim_moon = [-2.1e+05 83500 2.5e+05 4.84e+05];
    
    %--- CORPI CELESTI ---
    
    %[ P_E ] = draw_circle( 0,0.1,0,0,Re,2*pi);
    %patch(P_E(:,1)',P_E(:,2)',[0.4 0.8 0]);
    img_earth = imread('./assets/img_earth.jpg'); imagesc([-Re Re], [-Re Re],img_earth);
    img_moon = imread('./assets/img_moon.jpg');
    img_sat = imread('./assets/img_sat.jpg');
    
    %--- ORBITA PARCHEGGIO ---
    
    axis(axis_lim_earth); Rsc = 300;
    time_box = [axis_lim_earth(2)/100*85,axis_lim_earth(4)/100*90];
    t_park = 4*3600;
    Am = -Wm*t_andata - Wm*t_park;
    Ap = wp*t_park;
    theta = beta - Ap;
    vetRx_park = []; vetRy_park = [];
    
    while(t_park > 0)
        
        R = [rp*cos(theta),rp*sin(theta),0];
        theta = theta + wp*dt;
        vetRx_park = [vetRx_park,R(1)];
        vetRy_park = [vetRy_park,R(2)];
        
        plot(vetRx_park,vetRy_park,'b-');
        
        t_park = t_park - dt;
        
        Am = Am + dt*Wm;
        delete(hRS); delete(hM); delete(hT); delete(hSC);
        %[ P_M ] = draw_circle( 0,0.1,-D*sin(Am),D*cos(Am),Rm,2*pi);
        [ P_Rs ] = draw_circle( 0,0.1,-D*sin(Am),D*cos(Am),Rs,2*pi);
        %[ P_SC ] = draw_circle( 0,0.1,R(1),R(2),Rsc,2*pi);
        hRS = plot(P_Rs(:,1),P_Rs(:,2),'r:');
        %hM = patch(P_M(:,1)',P_M(:,2)',[0 0 0]);
        hM = imagesc([-Rm-D*sin(Am) Rm-D*sin(Am)], [-Rm+D*cos(Am) Rm+D*cos(Am)],img_moon);
        %hSC = patch(P_SC(:,1)',P_SC(:,2)',[0 0 0]);
        hSC = imagesc([-Rsc+R(1) Rsc+R(1)], [-Rsc+R(2) Rsc+R(2)],img_sat);
        
        txt_time = strcat('-',num2str(round(t_park/3600,0)),'h');
        hT = text(time_box(1),time_box(2),txt_time);
        
        pause(timestamp);
    end
    
    %--- ORBITA PARTENZA ---
    
    mu = muE;
    r1= sqrt(D^2+Rs^2 - 2*D*Rs*cos(lambda1));
    gamma1 = acos((D-Rs*cos(lambda1))/r1);
    R0 = [rp*cos(beta),rp*sin(beta),0];
    R1 = [-r1*sin(gamma1),r1*cos(gamma1),0];
    [V0,V1] = lambert(R0,R1,t_andata);
    vr0 = dot(R0, V0)/norm(R0);
    alpha0 =  2/norm(R0) - (norm(V0))^2/muE;
    Am = -Wm*t_andata;
    
    R = [0 0 0]; t = 0;
    vetRx_and = []; vetRy_and = [];
    
    while(norm(R) < norm(R1))
        
        if(t > 2500)
            axis(axis_lim_full);
            time_box = [axis_lim_full(2)/100*85,axis_lim_full(4)/100*90];
            Rsc = 3000;
        end
        
        x = kepler_U(t, norm(R0), vr0, alpha0);
        [f,g] = f_and_g(x, t, norm(R0), alpha0);
        R = f*R0 + g*V0;
        t = t + dt;
        
        vetRx_and = [vetRx_and,R(1)];
        vetRy_and = [vetRy_and,R(2)];
        
        plot(vetRx_and,vetRy_and,'b-');
        
        Am = Am + dt*Wm;
        delete(hRS); delete(hM); delete(hT); delete(hSC);
        %[ P_M ] = draw_circle( 0,0.1,-D*sin(Am),D*cos(Am),Rm,2*pi);
        [ P_Rs ] = draw_circle( 0,0.1,-D*sin(Am),D*cos(Am),Rs,2*pi);
        %[ P_SC ] = draw_circle( 0,0.1,R(1),R(2),Rsc,2*pi);
        hRS = plot(P_Rs(:,1),P_Rs(:,2),'r:');
        %hM = patch(P_M(:,1)',P_M(:,2)',[0 0 0]);
        %hSC = patch(P_SC(:,1)',P_SC(:,2)',[0 0 0]);
        hM = imagesc([-Rm-D*sin(Am) Rm-D*sin(Am)], [-Rm+D*cos(Am) Rm+D*cos(Am)],img_moon);
        hSC = imagesc([-Rsc+R(1) Rsc+R(1)], [-Rsc+R(2) Rsc+R(2)],img_sat);
        
        txt_time = strcat('+',num2str(round(t/3600,0)),'h');
        hT = text(time_box(1),time_box(2),txt_time);
        
        pause(timestamp);
    end
    
    %--- ORBITA LUNARE ---
    
    mu = muM;
    vinf_in = V1 - [-Vm,0,0];
    R1_M = [R1(1),R1(2)-D,0];
    vr1 = dot(R1_M, vinf_in)/norm(R1_M);
    alphaM = 2/norm(R1_M) -norm(vinf_in)^2/muM;
    
    t_tot = t;
    R = [0 0 0]; t = 60;
    vetRx_m = []; vetRy_m = [];
    
    axis(axis_lim_moon);
    time_box = [axis_lim_moon(2)/100*75,axis_lim_moon(4)/100*97];
    Rsc = 2000;
    
    while(norm(R) < Rs)
        
        x = kepler_U(t, norm(R1_M), vr1, alphaM);
        [f,g] = f_and_g(x, t, norm(R1_M), alphaM);
        R = f*R1_M + g*vinf_in;
        Am = Wm*t;
        t = t + dt;
        
        RR = [-D*sin(Am) + R(1),D*cos(Am) + R(2),0];
        vetRx_m = [vetRx_m,RR(1)];
        vetRy_m = [vetRy_m,RR(2)];
        plot(vetRx_m,vetRy_m,'b-');
        
        delete(hRS); delete(hM); delete(hT); delete(hSC);
        %[ P_M ] = draw_circle( 0,0.1,-D*sin(Am),D*cos(Am),Rm,2*pi);
        [ P_Rs ] = draw_circle( 0,0.1,-D*sin(Am),D*cos(Am),Rs,2*pi);
        %[ P_SC ] = draw_circle( 0,0.1,RR(1),RR(2),Rsc,2*pi);
        hRS = plot(P_Rs(:,1),P_Rs(:,2),'r:');
        %hM = patch(P_M(:,1)',P_M(:,2)',[0 0 0]);
        hM = imagesc([-Rm-D*sin(Am) Rm-D*sin(Am)], [-Rm+D*cos(Am) Rm+D*cos(Am)],img_moon);
        %hSC = patch(P_SC(:,1)',P_SC(:,2)',[0 0 0]);
        hSC = imagesc([-Rsc+RR(1) Rsc+RR(1)], [-Rsc+RR(2) Rsc+RR(2)],img_sat);
        
        txt_time = strcat('+',num2str(round((t+t_tot)/3600,0)),'h');
        hT = text(time_box(1),time_box(2),txt_time);
        
        pause(timestamp);
    end
    
    %--- ORBITA RITORNO ---
    
    [fdot, gdot] = fDot_and_gDot(x, norm(R), norm(R1_M), alphaM);
    vinf_out = fdot*R1_M + gdot*vinf_in;
    V2 = [-Vm*sin(pi/2-Am),-Vm*cos(pi/2-Am),0] + vinf_out;
    R2 = [vetRx_m(end),vetRy_m(end),0];
    
    axis(axis_lim_full);
    time_box = [axis_lim_full(2)/100*85,axis_lim_full(4)/100*90];
    Rsc = 3000;
    
    mu = muE;
    
    alphaR =  2/norm(R2) - (norm(V2))^2/muE;
    vr2 = dot(R2, V2)/norm(R2);
    t_tot = t_tot + t; t = 60;
    vetRx_ret = []; vetRy_ret = []; 
    prev = norm(R2); R = R2;
    
    while(norm(R) <= prev)
        
        if(norm(R) < 15000)
            axis(axis_lim_earth); Rsc = 100;
            time_box = [axis_lim_earth(2)/100*85,axis_lim_earth(4)/100*90];
            Rsc = 300;
        end
        
        prev = norm(R);
        x = kepler_U(t, norm(R2), vr2, alphaR); 
        [f, g] = f_and_g(x, t, norm(R2), alphaR);
        R = f*R2 + g*V2;
        t = t + dt;
        
        vetRx_ret = [vetRx_ret,R(1)];
        vetRy_ret = [vetRy_ret,R(2)];
        plot(vetRx_ret,vetRy_ret,'b-');
        
        Am = Am + Wm*dt;
        delete(hRS); delete(hM); delete(hT); delete(hSC);
        %[ P_M ] = draw_circle( 0,0.1,-D*sin(Am),D*cos(Am),Rm,2*pi);
        [ P_Rs ] = draw_circle( 0,0.1,-D*sin(Am),D*cos(Am),Rs,2*pi);
        %[ P_SC ] = draw_circle( 0,0.1,R(1),R(2),Rsc,2*pi);
        hRS = plot(P_Rs(:,1),P_Rs(:,2),'r:');
        %hM = patch(P_M(:,1)',P_M(:,2)',[0 0 0]);
        hM = imagesc([-Rm-D*sin(Am) Rm-D*sin(Am)], [-Rm+D*cos(Am) Rm+D*cos(Am)],img_moon);
        %hSC = patch(P_SC(:,1)',P_SC(:,2)',[0 0 0]);
        hSC = imagesc([-Rsc+R(1) Rsc+R(1)], [-Rsc+R(2) Rsc+R(2)],img_sat);
        
        txt_time = strcat('+',num2str(round((t+t_tot)/3600,0)),'h');
        hT = text(time_box(1),time_box(2),txt_time);
        
        pause(timestamp);
    end
    hold off;
    
    fprintf('\n --- Completato --- \n');

end