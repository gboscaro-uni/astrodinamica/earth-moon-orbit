function [] = esporta_excel(M,nomefile)
    

    fprintf('\n --- Esportazione risultati in Excel --- \n');

    X = zeros(length([M{:,1}]),9);

    X(:,1) = [M{:,1}];
    X(:,1) = round(X(:,1)/3600,0);
    X(:,2) = [M{:,2}];
    X(:,2) = round(X(:,2),4);
    X(:,3) = [M{:,3}];
    X(:,3) = round(X(:,3),4);
    X(:,5) = [M{:,11}];
    X(:,5) = round(X(:,5),1);
    X(:,6) = [M{:,12}];
    X(:,6) = round(X(:,6),1);
    X(:,7) = [M{:,13}];
    X(:,7) = round(X(:,7),2);
    X(:,8) = [M{:,14}];
    X(:,8) = round(X(:,8)/3600,0);
    X(:,9) = [M{:,15}];
    X(:,9) = round(X(:,9)/3600,0);
    for i = 1:1:length([M{:,1}])
        X(i,4) = norm(M{i,4});
    end
    X(:,4) = round(X(:,4),2);
    
    header=["Tempo Andata","Beta","Lambda1","V0","H Perigeo","H Periselenio","Delta V",'Tempo Totale','Tempo Periselenio']; 
    X = [header;X];
    xlswrite(strcat(nomefile,'.xls'),X);
    
    fprintf('\n --- Esportazione completata --- \n');
end