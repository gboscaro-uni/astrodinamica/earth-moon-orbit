function [ P ] = draw_circle( a,b,xg,yg,rg,t )
    iter=a:b:t;
    P0=[xg+rg*cos(a),yg+rg*sin(a)];
    P1=[xg+rg*cos(a+b),yg+rg*sin(a+b)]; 
    P=[P0;P1];
    for i=iter
        Pnew=[xg+2*cos(b)*(P(end,1)-xg)-(P(end-1,1)-xg),yg+2*cos(b)*(P(end,2)-yg)-(P(end-1,2)-yg)];
        P=[P;Pnew];
    end
end