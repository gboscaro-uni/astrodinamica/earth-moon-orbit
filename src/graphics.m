function [] = graphics(t_andata,R0,V0,alphaE,vetDD,vetRx_M,vetRy_M,vetRx_ret,vetRy_ret)

    fprintf('\n --- Tracciamento grafici --- \n');

    %PARAMETRI FISICI

    global muE; global muM;
    muE = 398600.44; %Costante Gravitazionale Terra [Km3/s2]
    muM = 4904; %Costante Gravitazionale Luna [Km3/s2]
    Re = 6378; %Raggio Terra [Km]
    Rm = 3476/2; %Raggio Luna [Km]
    D = 384400; %Distanza Terra-Luna [Km]
    Rs = D*(1/81)^(2/5); %Raggio sfera di influenza luna [Km]
    Vm = sqrt(muE/D); %Velocita luna [Km/s]
    Wm = Vm/D; %Velocita angolare luna [rad/s]

    %PARAMETRI ORBITALI

    hp = 400; %Altitudine parcheggio [Km]
    rp = Re + hp; %Raggio perigeo parcheggio [Km]
    
    %-------- GRAFICI --------

    figure(2); hold on;
    title('Free Return Orbit (Riferimento Geocentrico)');
    axis equal;

    %---PIANETI---
    
    img_earth = imread('./assets/img_earth.jpg');
    img_moon = imread('./assets/img_moon.jpg');

%     [ P_E ] = draw_circle( 0,0.1,0,0,Re,2*pi); %Cerchio Terra 
    [ P_park ] = draw_circle( 0,0.1,0,0,rp,2*pi); %Cerchio Orbita Parcheggio
%     [ P_M1 ] = draw_circle( 0,0.1,0,D,Rm,2*pi); %Cerchio Luna all arrivo
    [ P_Rs ] = draw_circle( 0,0.1,0,D,Rs,2*pi); %Cerchio Sfera influenza
    
%     patch(P_E(:,1)',P_E(:,2)',[0.4 0.8 0]);
    imagesc([-Re Re], [-Re Re],img_earth);
    imagesc([-Rm Rm], [D-Rm D+Rm],img_moon);
%     patch(P_M1(:,1)',P_M1(:,2)',[0 0 0]);
    plot(P_park(:,1),P_park(:,2),'r--');
    plot(P_Rs(:,1),P_Rs(:,2),'r:');

    %---ORBITA TRASFERIMENTO PARTENZA---

    vr0 = dot(R0, V0)/norm(R0);
    vetRx_and = []; vetRy_and = []; itmax_and = 100000;

    for k = linspace(0,t_andata,itmax_and)

        x = kepler_U(k, norm(R0), vr0, alphaE);
        [f, g] = f_and_g(x, k, norm(R0), alphaE);
        R = f*R0 + g*V0;

        vetRx_and = [vetRx_and,R(1)];
        vetRy_and = [vetRy_and,R(2)];

    end

    plot(vetRx_and,vetRy_and,'b-'); %Orbita
    axis equal;

    %---ORBITA LUNARE---

    vetRM_rot = zeros(length(vetDD),2);

    for z = 1:1:length(vetDD)

        %MR = [cos(vetDD(z)) sin(vetDD(z)) 0;-sin(vetDD(z)) cos(vetDD(z)) 0; 0 0 1];
        RR = [vetRx_M(z) - D*sin(vetDD(z)),vetRy_M(z) + D*cos(vetDD(z)),0];
        %temp = MR'*RR';
        vetRM_rot(z,1) = RR(1);
        vetRM_rot(z,2) = RR(2);

    end

    %ORBITA LUNARE GEOCENTRICA
    plot(vetRM_rot(:,1),vetRM_rot(:,2),'b-');

    %---ORBITA RITORNO---

    plot(vetRx_ret,vetRy_ret,'b-'); %Orbita Ritorno

    %PIANETI POSIZIONE FINALE
%     [ P_M2 ] = draw_circle( 0,0.1,-D*sin(vetDD(end)),D*cos(vetDD(end)),Rm,2*pi); %Posizione Luna finale
    [ P_Rs ] = draw_circle( 0,0.1,-D*sin(vetDD(end)),D*cos(vetDD(end)),Rs,2*pi); %Sfera influenza finale
%     [ P_LimMax ] = draw_circle( 0,0.1,0,0,Re+1000,2*pi); %Limiti rientro max
%     [ P_LimMin ] = draw_circle( 0,0.1,0,0,Re+300,2*pi); %Limiti rientro min
%     patch(P_M2(:,1)',P_M2(:,2)',[0 0 0]);
    imagesc([-D*sin(vetDD(end))-Rm -D*sin(vetDD(end))+Rm], [D*cos(vetDD(end))-Rm D*cos(vetDD(end))+Rm],img_moon);
    plot(P_Rs(:,1),P_Rs(:,2),'r:');
%     plot(P_LimMax(:,1),P_LimMax(:,2),'g.-');
%     plot(P_LimMin(:,1),P_LimMin(:,2),'g.-');

    %PIANETI POSIZIONE BURN-OUT
    %[ P_M0 ] = draw_circle( 0,0.1,-D*sin(-Wm*t_andata),D*cos(-Wm*t_andata),Rm,2*pi); %Posizione Luna bo
    [ P_Rs ] = draw_circle( 0,0.1,-D*sin(-Wm*t_andata),D*cos(-Wm*t_andata),Rs,2*pi); %Sfera influenza bo
    %patch(P_M0(:,1)',P_M0(:,2)',[0 0 0]);
    imagesc([-D*sin(-Wm*t_andata)-Rm -D*sin(-Wm*t_andata)+Rm], [D*cos(-Wm*t_andata)-Rm D*cos(-Wm*t_andata)+Rm],img_moon);
    
    plot(P_Rs(:,1),P_Rs(:,2),'r:');

    %ORBITA LUNA
%     [ O_M_sup ] = draw_circle( 0,0.1,0,0,D+Rm,2*pi);
%     [ O_M_inf ] = draw_circle( 0,0.1,0,0,D-Rm,2*pi);
    %[ O_M_orb ] = draw_circle( 0,0.1,0,0,D,2*pi);
    %plot(O_M_orb(:,1),O_M_orb(:,2),'k-');
%     plot(O_M_inf(:,1),O_M_inf(:,2),'g--');
%     plot(O_M_sup(:,1),O_M_sup(:,2),'g--');

    hold off;

    %ORBITA LUNARE MOONCENTRICA
    figure(3); hold on;
    title('FlyBy Orbit (Riferimento Mooncentrico)');
    %[ P_MM ] = draw_circle( 0,0.1,0,0,Rm,2*pi);
    imagesc([-Rm Rm], [-Rm Rm],img_moon);
    plot(vetRx_M,vetRy_M,'b-');
    %patch(P_MM(:,1)',P_MM(:,2)',[0 0 0]);
    
    axis equal;
    hold off;
    
    fprintf('\n --- Completato --- \n');

end