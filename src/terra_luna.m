clc;
tic;

%PARAMETRI FISICI

global muE; global muM; global mu; global D; global Rs; global Vm; global Re; global Rm; global Wm; global rp; global vp;
muE = 398600.44; %Costante Gravitazionale Terra [Km3/s2]
muM = 4904; %Costante Gravitazionale Luna [Km3/s2]
Re = 6378; %Raggio Terra [Km]
Rm = 3476/2; %Raggio Luna [Km]
D = 384400; %Distanza Terra-Luna [Km]
Rs = D*(1/81)^(2/5); %Raggio sfera di influenza luna [Km]
Vm = sqrt(muE/D); %Velocita luna [Km/s]
Wm = Vm/D; %Velocita angolare luna [rad/s]

%PARAMETRI ORBITALI

hp = 400; %Altitudine parcheggio [Km]
rp = Re + hp; %Raggio perigeo parcheggio [Km]
vp = sqrt(muE/rp); %Velocita perigeo parcheggio [Km/s]

%PARAMETRI ITERATIVI

M_Risultati = cell(1,15); %matrice delle orbite che hanno avuto successo
n_successi = 0; %numero di successi

dbb = 50; dll = 20; dtt = 20; %numero intervalli iterazioni
it_tot = dbb*dll*dtt; %iterazioni totali

cicli_completati = 0;

for bb = linspace(22.5,23,dbb)
    for ll = linspace(44.3,44.5,dll)
        for tt = linspace(54.5,55.5,dtt)
            
            cicli_completati = cicli_completati +1;
            
            %FLAG
            prec = inf; impatto_luna = false; impatto_terra = false;
            
            %PARAMETRI ITERATIVI
            beta = (bb)*(pi/180); %angolo di burnout [rad]
            lambda1 = (ll)*(pi/180); %angolo di arrivo alla SI della luna [rad]
            t_andata = tt*3600; %Tempo di trasferimento Terra - Luna [s]

            aM = Wm*t_andata; %angolo spazzato dalla luna all andata [rad]
            r1= sqrt(D^2+Rs^2 - 2*D*Rs*cos(lambda1)); %modulo raggio posizione finale [Km]
            gamma1 = acos((D-Rs*cos(lambda1))/r1); %angolo del raggio geocentrico all'arrivo [rad]

            R0 = [rp*cos(beta),rp*sin(beta),0]; %Vettore posizione iniziale [km]
            R1 = [-r1*sin(gamma1),r1*cos(gamma1),0]; %Vettore posizione finale [km]

            %--- ORBITA DI PARTENZA ---

            mu = muE; %orbita Terreste

            [V0,V1] = lambert(R0,R1,t_andata); %velocita di partenza e arrivo, riferimento geocentrico [km/s]
            alphaE =  2/norm(R0) - (norm(V0))^2/muE; %1/a orbita di partenza
            vcirc_x = vp*sin(beta); %componente x velocita di parcheggio circolare [km/s]
            vcirc_y = vp*cos(beta); %componente y velocita di parcheggio circolare [km/s]
            deltaV = norm([abs(V0(1)-vcirc_x),abs(abs(V0(2)-vcirc_y))]); %deltaV di partenza [km/s]
            
            % --- ORBITA LUNARE ---

            mu = muM; %orbita lunare

            vinf_in = V1 - [-Vm,0,0]; %Velocita in ingresso rispetto alla luna [km/s]
            R1_M = [R1(1),R1(2)-D,0]; %Raggio di arrivo rispetto alla Luna [km]
            vr1 = dot(R1_M, vinf_in)/norm(R1_M); %velocita radiale in arrivo [km/s]

            %phi1 = gamma1 + acos(abs(V1(1))/norm(V1));
            % ahyp =  1/(2/norm(RR1) - (norm(vinf_in))^2/muM); %semiasse maggiore iperbolica lunare
            % e2 = asin( (Vm*cos(lambda1) - norm(vinf_in)*cos(lambda1+gamma1-phi1))/norm(vinf_in) ); %angolo di puntamento lunare
            % Enhyp = norm(vinf_in)^2/2 - muM/Rs;
            % hhyp = Rs*norm(vinf_in)*sin(e2); %momento angolare iperbolica lunare
            % ehyp = sqrt(1 + 2*Enhyp*hhyp^2/muM^2); %eccentricita iperbolica lunare
            % rpM = (hhyp^2/muM)*(1/(1+ehyp)); %periselenio
            % vpM = sqrt(2*(Enhyp + muM/rpM)); %velocita periselenio
            % deltaM = 2*asin(1/ehyp); %turn angle
            % thetaS = acos((1/ehyp)*(hhyp^2/(Rs*muM)-1));
            % F = 2*atanh(tan(thetaS/2)*sqrt((ehyp-1)/(ehyp+1)));
            % Mh = ehyp*sinh(F)-F;
            % ttot = 2*Mh*sqrt(hhyp^3/muM);

            
            alphaM = 2/norm(R1_M) -norm(vinf_in)^2/muM; %1/a orbita lunare
            i = 0; itmax_M = 5000;
            vetRx_M = zeros(1,itmax_M); vetRy_M = zeros(1,itmax_M); vetR_abs = zeros(1,itmax_M); vetDD = zeros(1,itmax_M);
            t_flyby = 60; R = [0,0,0]; dt = 1200;
            fR_M = 1500; %margine di sicurezza [km]

            while(norm(R) < Rs && i < itmax_M)
                
                i = i+1;

                x = kepler_U(t_flyby, norm(R1_M), vr1, alphaM); %anomalia universale al tempo t
                [f,g] = f_and_g(x, t_flyby, norm(R1_M), alphaM); %coefficienti lagrangiani
                R = f*R1_M + g*vinf_in; %raggio mooncentrico al tempo t
                
                %verifico di non impattare nella luna
                if(norm(R) < Rm)
                    disp('Impatto con la superficie LUNA');
                    impatto_luna = true;
                    break;
                end
                
                vetR_abs(i) = norm(R);
                vetRx_M(i) = R(1);
                vetRy_M(i) = R(2);
                vetDD(i) = Wm*t_flyby;
                
                if(i > 1 && norm(vetR_abs(i))+abs(norm(vetR_abs(i))-norm(vetR_abs(i-1)))+fR_M > Rs)
                    dt = 60;
                end
                
                t_flyby = t_flyby+dt;
            end
            vetRx_M(vetRx_M==0) = [];
            vetRy_M(vetRy_M==0) = [];
            vetR_abs(vetR_abs==0) = [];
            vetDD(vetDD==0) = [];
            
            hp_M = min(vetR_abs) - Rm; %altezza periasse lunare
            t_hp = t_flyby/2;
            
            %se non impatto con la luna, computo l orbita di ritorno
            if(not(impatto_luna))
            
                [fdot, gdot] = fDot_and_gDot(x, norm(R), norm(R1_M), alphaM); %coefficienti lagrangiani velocita
                vinf_out = fdot*R1_M + gdot*vinf_in; %velocita in uscita dalla SI luna [km/s]

                %ROTAZIONE

                %Cambio coordinate velocita rispetto al riferimento
                %geocentrico
                V2 = [-Vm*sin(pi/2-vetDD(end)),-Vm*cos(pi/2-vetDD(end)),0] + vinf_out;
                R2 = [vetRx_M(end),vetRy_M(end),0];
                V2_geo = V2;
                R2_geo = [-D*sin(vetDD(end)) + R2(1),D*cos(vetDD(end)) + R2(2),0];

                %--- ORBITA RITORNO ---

                mu = muE; %orbita geocenrica
% 
%                 arit =  1/(2/norm(R2_geo) - (norm(V2_geo))^2/muE); %semiasse maggiore orbita ritorno
%                 Enrit = norm(V2_geo)^2/2 - muE/norm(R2_geo); %energia orbita ritorno
%                 hrit = norm(cross(R2_geo,V2_geo)); %momento angolare orbita ritorno
%                 erit = sqrt(1 + 2*Enrit*hrit^2/muE^2); %eccentricita orbita ritorno
%                 rprit = (hrit^2/muE)*(1/(1+erit)); %perigeo ritorno

                alphaR =  2/norm(R2_geo) - (norm(V2_geo))^2/muE; %1/a orbita di ritorno
                vr2 = dot(R2_geo, V2_geo)/norm(R2_geo); %velocita radiale iniziale orbita di ritorno
                t_ritorno = 60; dt = 1200; itmax_ret = 5000;
                vetRx_ret = zeros(1,itmax_ret); vetRy_ret = zeros(1,itmax_ret); 
                prec = inf; R = [inf,inf,inf]; j = 0; fR_E = 10000;

                while(j < itmax_ret)

                    j = j+1;
                    
                    x = kepler_U(t_ritorno, norm(R2_geo), vr2, alphaR); %variabile universale
                    [f, g] = f_and_g(x, t_ritorno, norm(R2_geo), alphaR); %coefficienti lagrangiani
                    R = f*R2_geo + g*V2_geo; %raggio all istante t
                    vetRx_ret(j) = R(1);
                    vetRy_ret(j) = R(2);
                    
                    %controllo di non impattare con la terra
                    if(norm(R) < Re)
                        disp('Impatto con la superficie TERRA');
                        impatto_terra = true;
                        break;
                    end
                    %se passo il perigeo mi fermo
                    if(norm(R) > prec)
                        break;
                    end
                    if(j > 1 && norm(R) - abs(norm(R)-prec)-fR_E < Re+1000)
                        dt = 60;
                    end
                    prec = norm(R);
                    
                    t_ritorno = t_ritorno+dt;
                end
                vetRx_ret(vetRx_ret==0) = [];
                vetRy_ret(vetRy_ret==0) = [];
                
                hp_finale = prec - Re; %altezza perigeo di arrivo al ritorno
                
                %Se l orbita rispetta i requisiti viene salvata
                if(hp_finale >= 300 && hp_finale <= 1000 && not(impatto_terra) && not(impatto_luna))
                    
                    n_successi = n_successi + 1;
                    M_Risultati{n_successi,1} = t_andata;
                    M_Risultati{n_successi,2} = beta;
                    M_Risultati{n_successi,3} = lambda1;
                    M_Risultati{n_successi,4} = V0;
                    M_Risultati{n_successi,5} = alphaE;
                    M_Risultati{n_successi,6} = vetDD;
                    M_Risultati{n_successi,7} = vetRx_M;
                    M_Risultati{n_successi,8} = vetRy_M;
                    M_Risultati{n_successi,9} = vetRx_ret;
                    M_Risultati{n_successi,10} = vetRy_ret;
                    M_Risultati{n_successi,11} = hp_finale;
                    M_Risultati{n_successi,12} = hp_M;
                    M_Risultati{n_successi,13} = deltaV;
                    M_Risultati{n_successi,14} = t_andata+t_flyby+t_ritorno;
                    M_Risultati{n_successi,15} = t_andata + t_hp;
                end
                
                fprintf('\n --- in corso - completatamento %.2f %% --- \n',cicli_completati/it_tot*100);
            end
        end
    end
end

fprintf('\n --- Salvataggio --- \n');

save('./out/data_results.mat', 'M_Risultati');

fprintf('\n --- Completato --- \n');

toc;